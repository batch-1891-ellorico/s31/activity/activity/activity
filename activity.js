// ACTIVTY

// QUESTIONS
// 1. What directive is used by Node.js in loading the modules it needs?
	// The required directive
// 2. What Node.js module contains a method for server creation?
	// The http module
// 3. What is the method of the http object responsible for creating a server using Node.js?
	// The createServer
// 4. What method of the response object allows us to set status codes and content types?
	// The writeHead method
// 5. Where will console.log() output its contents when run in Node.js?
	// On the terminal
// 6. What property of the request object contains the address' endpoint?
	// The request.url



// #5 Import http module using required directive
const http = require('http')

// #6 Create a variable port and and assign value of 3000
const port = 3000

// #7 Create a server using createServer method for port provided
const server = http.createServer(function(request, response) {
	// #9 Condition for log-in
	if (request.url =='/login') {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('You are on the log-in page!')
	// #11 Create condition to return error message
	} else {
		response.writeHead(404, {'Content-Type': 'text/plain'})
		response.end('Error loading: Page not available! :(')
	}
})
server.listen(port)

// #8 Console log in the terminal when successful
console.log(`Server is running at localhost: ${port}`)